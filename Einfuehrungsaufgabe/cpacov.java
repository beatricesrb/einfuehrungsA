
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import java.util.HashMap;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

public class cpacov {

    public static void main(final String args[]) throws IOException {

        File cFile = null;
        File argFile = null;

        if (args.length > 0) {
           
            argFile = new File(args[0]);
            cFile = new File(args[1]);
        } else {
            System.err.println("Invalid arguments count:" + args.length);
        }

        
        BufferedReader argbr = new BufferedReader(new FileReader(argFile));
        BufferedReader cbr = new BufferedReader(new FileReader(cFile));
        PrintWriter out = new PrintWriter(new FileWriter(new File("lock-loop.c.cov"), true));

        String cLine;
        String argLine;
        int lineNum = 0;

        LinkedHashSet<Integer> hashSet = new LinkedHashSet<>();
        Map<Integer, String> hits = new HashMap<>();
        Map<Integer, Integer> map = new HashMap<>();

        while ((argLine = argbr.readLine()) != null) {

            if (argLine.contains("Line")) {
                String answer = argLine.substring(argLine.indexOf("\"") + 1, argLine.indexOf("\\l\""));
          
                int iPos = answer.indexOf(' ');
                String lNummer = answer.substring(iPos, answer.length()).trim().replaceAll("\\\\l", "")
                        .replaceAll("\\\\", ""); // LineNumber:Code
                int v = lNummer.indexOf(":");
                int number = Integer.parseInt(lNummer.substring(0, v));
             
                if (number > 0) {
                    hashSet.add(number);
                }

            }

        }
       
        while ((cLine = cbr.readLine()) != null) {
            if (cLine.length() >= 0) {
                lineNum++;
            }

            hits.put(lineNum, cLine);

        }

        ArrayList<Integer> listWithoutDuplicates = new ArrayList<>(hashSet);

        for (Integer r : listWithoutDuplicates) {
            if (map.containsKey(r)) {
                map.put(r, map.get(r) + 1);
            } else {
                map.put(r, 1);
            }
        }

        
      
        out.println("-:  0:Source:../cpachecker/test/programs/simple/lock-loop.c ");
        out.println("-:  0:Graph:ARG.dot");
        out.println("-:  0:Runs:1");
        Iterator<Integer> iterator = hits.keySet().iterator();
        
		while (iterator.hasNext() ) {
            Integer key = iterator.next();
             if(map.containsKey(key)){  
                out.println(map.get(key)+":  "+key+":"+hits.get(key));
            } else {
                 out.println("-:  "+key+":"+hits.get(key));
            }
            
        }

        System.out.println("lock-loop.c.cov - File was successfully created");
        out.close();
        cbr.close();
        argbr.close();

    }

}